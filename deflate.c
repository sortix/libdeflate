/*
 * Copyright (c) 2015 Jonas 'Sortie' Termansen
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define Z_CONST
#include <zlib.h>
#ifndef z_const
#define z_const
#endif

#include <deflate.h>

// The zlib API has serious issues with using poor datatypes like unsigned int
// and unsigned long instead of proper types like size_t. Unsigned long
// parameters are sometimes even truncated to unsigned int. There might be
// overflow issues on buffers near UINT_MAX. Until the codebase is audited and
// we know for sure that UINT_MAX-sized buffers are safe, let's not go anywhere
// near that size.
static const size_t Z_SAFE_BUFFER_BOUND = UINT_MAX / 16 + 1;

static int deflate_error_of_z_error(int zerr)
{
	switch ( zerr ) {
	case Z_NEED_DICT: return DEFLATE_NEED_DICT;
	case Z_STREAM_END: return DEFLATE_STREAM_END;
	case Z_OK: return 0;
	case Z_ERRNO:
		switch ( errno ) {
		#ifdef ENOMEM
		case ENOMEM: return DEFLATE_EMEM;
		#endif
		default: return DEFLATE_EUNKNOWN;
		}
	case Z_STREAM_ERROR: return DEFLATE_ESTREAM;
	case Z_DATA_ERROR: return DEFLATE_EDATA;
	case Z_MEM_ERROR: return DEFLATE_EMEM;
	case Z_BUF_ERROR: return DEFLATE_EBUF;
	default: return DEFLATE_EUNKNOWN;
	}
}

static bool is_valid_flush(int flush)
{
	switch ( flush ) {
	case DEFLATE_FLUSH_NO:
	case DEFLATE_FLUSH_PARTIAL:
	case DEFLATE_FLUSH_SYNC:
	case DEFLATE_FLUSH_FULL:
	case DEFLATE_FLUSH_FINISH:
	case DEFLATE_FLUSH_BLOCK:
	case DEFLATE_FLUSH_TREES:
		return true;
	}
	return false;
}

static int z_flush_of_deflate_flush(int flush)
{
	switch ( flush ) {
	case DEFLATE_FLUSH_NO: return Z_NO_FLUSH;
	case DEFLATE_FLUSH_PARTIAL: return Z_PARTIAL_FLUSH;
	case DEFLATE_FLUSH_SYNC: return Z_SYNC_FLUSH;
	case DEFLATE_FLUSH_FULL: return Z_FULL_FLUSH;
	case DEFLATE_FLUSH_FINISH: return Z_FINISH;
	case DEFLATE_FLUSH_BLOCK: return Z_BLOCK;
	case DEFLATE_FLUSH_TREES: return Z_TREES;
	}
	return 0;
}

enum deflate_begun_kind
{
	DEFLATE_BEGUN_KIND_NONE,
	DEFLATE_BEGUN_KIND_COMPRESS,
	DEFLATE_BEGUN_KIND_COMPRESS_RAW,
	DEFLATE_BEGUN_KIND_DECOMPRESS,
	DEFLATE_BEGUN_KIND_DECOMPRESS_RAW,
};

struct deflate_stream
{
	z_stream zs;
	int level;
	int strategy;
	int windowbits;
	int memlevel;
	enum deflate_begun_kind begun_kind;
};

struct deflate_stream *deflate_create(void)
{
	struct deflate_stream *result =
		(struct deflate_stream *) calloc(1, sizeof(struct deflate_stream));
	if ( !result )
		return NULL;
	deflate_end(result);
	return result;
}

struct deflate_stream *deflate_create_compress(int level)
{
	struct deflate_stream *result = deflate_create();
	if ( !result )
		return NULL;
	deflate_set_level(result, level);
	if ( deflate_begin_compress(result) )
		return deflate_destroy(result), (struct deflate_stream *) NULL;
	return result;
}

struct deflate_stream *deflate_create_compress_raw(int level)
{
	struct deflate_stream *result = deflate_create();
	if ( !result )
		return NULL;
	deflate_set_level(result, level);
	if ( deflate_begin_compress_raw(result) )
		return deflate_destroy(result), (struct deflate_stream *) NULL;
	return result;
}

struct deflate_stream *deflate_create_decompress(void)
{
	struct deflate_stream *result = deflate_create();
	if ( !result )
		return NULL;
	if ( deflate_begin_decompress(result) )
		return deflate_destroy(result), (struct deflate_stream *) NULL;
	return result;
}

struct deflate_stream *deflate_create_decompress_raw(void)
{
	struct deflate_stream *result = deflate_create();
	if ( !result )
		return NULL;
	if ( deflate_begin_decompress_raw(result) )
		return deflate_destroy(result), (struct deflate_stream *) NULL;
	return result;
}

void deflate_destroy(struct deflate_stream *stream)
{
	if ( !stream )
		return;
	deflate_end(stream);
	free(stream);
}

void deflate_set_level(struct deflate_stream *stream,
                       int level)
{
	assert(stream);
	stream->level = level;
}

int deflate_get_level(struct deflate_stream *stream)
{
	assert(stream);
	return stream->level;
}

void deflate_set_strategy(struct deflate_stream *stream,
                          int strategy)
{
	assert(stream);
	stream->strategy = strategy;
}

int deflate_get_strategy(struct deflate_stream *stream)
{
	assert(stream);
	return stream->strategy;
}

void deflate_set_windowbits(struct deflate_stream *stream,
                            int windowbits)
{
	assert(stream);
	if ( 15 < windowbits )
		windowbits = 15;
	if ( windowbits < -15 )
		windowbits = -15;
	if ( windowbits < 0 )
		windowbits = -windowbits;
	stream->windowbits = windowbits;
}

int deflate_get_windowbits(struct deflate_stream *stream)
{
	assert(stream);
	return stream->windowbits;
}

void deflate_set_memlevel(struct deflate_stream *stream,
                          int memlevel)
{
	assert(stream);
	stream->memlevel = memlevel;
}

int deflate_get_memlevel(struct deflate_stream *stream)
{
	assert(stream);
	return stream->memlevel;
}

int deflate_begin_compress(struct deflate_stream *stream)
{
	assert(stream);
	if ( stream->begun_kind != DEFLATE_BEGUN_KIND_NONE )
		return DEFLATE_EUSAGE;
	memset(&stream->zs, 0, sizeof(stream->zs));
	int err = deflateInit2(&stream->zs, stream->level, Z_DEFLATED,
	                       stream->windowbits, stream->memlevel,
	                       stream->strategy);
	if ( err != Z_OK )
		return deflate_error_of_z_error(err);
	stream->begun_kind = DEFLATE_BEGUN_KIND_COMPRESS;
	return 0;
}

int deflate_begin_compress_raw(struct deflate_stream *stream)
{
	assert(stream);
	if ( stream->begun_kind != DEFLATE_BEGUN_KIND_NONE )
		return DEFLATE_EUSAGE;
	memset(&stream->zs, 0, sizeof(stream->zs));
	int err = deflateInit2(&stream->zs, stream->level, Z_DEFLATED,
	                       -stream->windowbits, stream->memlevel,
	                       stream->strategy);
	if ( err != Z_OK )
		return deflate_error_of_z_error(err);
	stream->begun_kind = DEFLATE_BEGUN_KIND_COMPRESS_RAW;
	return 0;
}

int deflate_begin_decompress(struct deflate_stream *stream)
{
	assert(stream);
	if ( stream->begun_kind != DEFLATE_BEGUN_KIND_NONE )
		return DEFLATE_EUSAGE;
	memset(&stream->zs, 0, sizeof(stream->zs));
	int err = inflateInit2(&stream->zs, stream->windowbits);
	if ( err != Z_OK )
		return deflate_error_of_z_error(err);
	stream->begun_kind = DEFLATE_BEGUN_KIND_DECOMPRESS;
	return 0;
}

int deflate_begin_decompress_raw(struct deflate_stream *stream)
{
	assert(stream);
	if ( stream->begun_kind != DEFLATE_BEGUN_KIND_NONE )
		return DEFLATE_EUSAGE;
	memset(&stream->zs, 0, sizeof(stream->zs));
	int err = inflateInit2(&stream->zs, -stream->windowbits);
	if ( err != Z_OK )
		return deflate_error_of_z_error(err);
	stream->begun_kind = DEFLATE_BEGUN_KIND_DECOMPRESS_RAW;
	return 0;
}

int deflate_set_dictionary(struct deflate_stream *stream,
                           const unsigned char *dictionary,
                           size_t dictionary_length)
{
	assert(stream);
	if ( (unsigned int) dictionary_length != dictionary_length )
		return DEFLATE_EMEM;
	int err = 0;
	switch ( stream->begun_kind ) {
	case DEFLATE_BEGUN_KIND_NONE:
		err = DEFLATE_EUSAGE;
		break;
	case DEFLATE_BEGUN_KIND_COMPRESS:
	case DEFLATE_BEGUN_KIND_COMPRESS_RAW:
		err = deflateSetDictionary(&stream->zs, dictionary,
		                           (unsigned int) dictionary_length);
		err = deflate_error_of_z_error(err);
		break;
	case DEFLATE_BEGUN_KIND_DECOMPRESS:
	case DEFLATE_BEGUN_KIND_DECOMPRESS_RAW:
		err = inflateSetDictionary(&stream->zs, dictionary,
		                           (unsigned int) dictionary_length);
		err = deflate_error_of_z_error(err);
		break;
	}
	return err;
}

int deflate_get_dictionary(struct deflate_stream *stream,
                           unsigned char *dictionary,
                           size_t *dictionary_length)
{
	assert(stream);
	int err = 0;
	size_t dictionary_length_st = *dictionary_length;
	if ( UINT_MAX <= dictionary_length_st )
		dictionary_length_st = UINT_MAX;
	unsigned int dictionary_length_ui = dictionary_length_st;
	switch ( stream->begun_kind ) {
	case DEFLATE_BEGUN_KIND_NONE:
	case DEFLATE_BEGUN_KIND_COMPRESS:
	case DEFLATE_BEGUN_KIND_COMPRESS_RAW:
		err = DEFLATE_EUSAGE;
		break;
	case DEFLATE_BEGUN_KIND_DECOMPRESS:
	case DEFLATE_BEGUN_KIND_DECOMPRESS_RAW:
		err = inflateGetDictionary(&stream->zs, dictionary,
		                           &dictionary_length_ui);
		err = deflate_error_of_z_error(err);
		break;
	}
	*dictionary_length = dictionary_length_ui;
	return err;
}

uint32_t deflate_get_adler32(struct deflate_stream *stream)
{
	assert(stream);
	return stream->zs.adler;
}

size_t deflate_compress_bound(struct deflate_stream *stream,
                              size_t uncompressed_size)
{
	unsigned long worst_compressed_size;
	if ( stream && stream->begun_kind != DEFLATE_BEGUN_KIND_NONE )
		worst_compressed_size = deflateBound(&stream->zs, uncompressed_size);
	else
		worst_compressed_size = deflateBound(NULL, uncompressed_size);
	// zlib doesn't protect against overflow. Try to poorly make up for it after
	// the fact. We know it at least overflowed an unsigned long so return that.
	if ( worst_compressed_size < uncompressed_size )
		return ULONG_MAX;
	return worst_compressed_size;
}

int deflate_process(struct deflate_stream *stream,
                    const unsigned char *input,
                    size_t input_size,
                    size_t *input_used,
                    unsigned char *output,
                    size_t output_size,
                    size_t *output_used,
                    int flush_type)
{
	assert(stream);
	if ( input_used )
		*input_used = 0;
	if ( output_used )
		*output_used = 0;
	if ( stream->begun_kind == DEFLATE_BEGUN_KIND_NONE )
		return DEFLATE_EUSAGE;
	if ( !is_valid_flush(flush_type) )
		return DEFLATE_EINVAL;
	if ( !input || !output || !input_used || !output_used )
		return DEFLATE_EINVAL;
	// A safety measure until we know zlib safely handles buffer sizes all the
	// way up to UINT_MAX without potential overflow issues.
	if ( Z_SAFE_BUFFER_BOUND < input_size )
		input_size = Z_SAFE_BUFFER_BOUND;
	if ( Z_SAFE_BUFFER_BOUND < output_size )
		output_size = Z_SAFE_BUFFER_BOUND;
	// Handle the backend API using unsigned int rather than size_t.
	if ( UINT_MAX < input_size )
		input_size = UINT_MAX;
	if ( UINT_MAX < output_size )
		output_size = UINT_MAX;
	stream->zs.next_in = (z_const unsigned char *) input;
	stream->zs.avail_in = (unsigned int) input_size;
	stream->zs.total_in = 0;
	stream->zs.next_out = output;
	stream->zs.avail_out = (unsigned int) output_size;
	stream->zs.total_out = 0;
	int err = Z_OK;
	switch ( stream->begun_kind ) {
	case DEFLATE_BEGUN_KIND_NONE: break;
	case DEFLATE_BEGUN_KIND_COMPRESS:
	case DEFLATE_BEGUN_KIND_COMPRESS_RAW:
		err = deflate(&stream->zs, z_flush_of_deflate_flush(flush_type));
		err = deflate_error_of_z_error(err);
		break;
	case DEFLATE_BEGUN_KIND_DECOMPRESS:
	case DEFLATE_BEGUN_KIND_DECOMPRESS_RAW:
		err = inflate(&stream->zs, z_flush_of_deflate_flush(flush_type));
		err = deflate_error_of_z_error(err);
		break;
	}
	size_t input_processed = input_size - stream->zs.avail_in;
	size_t output_processed = output_size - stream->zs.avail_out;
	*input_used = input_processed;
	*output_used = output_processed;
	return err;
}

int deflate_loop(struct deflate_stream *stream,
                 void *reader_context,
                 int (*reader)(void *context,
                               unsigned char *buffer,
                               size_t count,
                               size_t *transferred),
                 void *writer_context,
                 int (*writer)(void *context,
                               const unsigned char *buffer,
                               size_t count,
                               size_t *transferred))
{
	assert(stream);
	if ( stream->begun_kind == DEFLATE_BEGUN_KIND_NONE )
		return DEFLATE_EUSAGE;
	if ( !reader || !writer )
		return DEFLATE_EINVAL;
	size_t work_size = 512 * 1024;
	unsigned char *work = (unsigned char*) malloc(work_size);
	if ( !work )
		return DEFLATE_EMEM;
	int err = Z_OK;
	while ( true ) {
		unsigned char *in = work;
		size_t in_request = work_size / 2;
		size_t in_size = 0;
		int readerr = reader(reader_context, in, in_request, &in_size);
		if ( readerr != 0 && readerr != DEFLATE_STREAM_END ) {
			free(work);
			return DEFLATE_ECALLBACK;
		}
		int flush = DEFLATE_FLUSH_NO;
		if ( readerr == DEFLATE_STREAM_END )
			flush = DEFLATE_FLUSH_FINISH;
		else if ( in_size == 0 )
			break;
		do {
			unsigned char *out = work + in_size;
			size_t out_size = work_size - in_size;
			size_t used;
			size_t made;
			err = deflate_process(stream, in, in_size, &used, out, out_size,
				                  &made, flush);
			if ( err != 0 && err != DEFLATE_STREAM_END ) {
				free(work);
				return err;
			}
			in += used;
			in_size -= used;
			while ( made )
			{
				size_t transferred = 0;
				int writeerr = writer(writer_context, out, made, &transferred);
				if ( writeerr == DEFLATE_STREAM_END ||
					 (writeerr == 0 && transferred == 0) ) {
					free(work);
					return 0;
				}
				if ( writeerr != 0 ) {
					free(work);
					return DEFLATE_ECALLBACK;
				}
				out += transferred;
				made -= transferred;
			}
		} while ( in_size );
		if ( readerr == DEFLATE_STREAM_END )
			break;
	}
	free(work);
	return err;
}

void deflate_reset(struct deflate_stream *stream)
{
	assert(stream);
	switch ( stream->begun_kind ) {
	case DEFLATE_BEGUN_KIND_NONE: break;
	case DEFLATE_BEGUN_KIND_COMPRESS: deflateReset(&stream->zs); break;
	case DEFLATE_BEGUN_KIND_COMPRESS_RAW: deflateReset(&stream->zs); break;
	case DEFLATE_BEGUN_KIND_DECOMPRESS: inflateReset(&stream->zs); break;
	case DEFLATE_BEGUN_KIND_DECOMPRESS_RAW: inflateReset(&stream->zs); break;
	}
}

/* TODO: Perhaps we should forward the zlib error conditions here! */
void deflate_end(struct deflate_stream *stream)
{
	assert(stream);
	switch ( stream->begun_kind ) {
	case DEFLATE_BEGUN_KIND_NONE: break;
	case DEFLATE_BEGUN_KIND_COMPRESS: deflateEnd(&stream->zs); break;
	case DEFLATE_BEGUN_KIND_COMPRESS_RAW: deflateEnd(&stream->zs); break;
	case DEFLATE_BEGUN_KIND_DECOMPRESS: inflateEnd(&stream->zs); break;
	case DEFLATE_BEGUN_KIND_DECOMPRESS_RAW: inflateEnd(&stream->zs); break;
	}
	memset(stream, 0, sizeof(*stream));
	stream->level = DEFLATE_LEVEL_DEFAULT;
	stream->strategy = Z_DEFAULT_STRATEGY;
	stream->windowbits = 15;
	stream->memlevel = 8;
}

const char *deflate_strerror(int errnum)
{
	switch ( errnum ) {
	default:
	case DEFLATE_EUNKNOWN: return "unknown error";
	case DEFLATE_ESTREAM: return "stream error";
	case DEFLATE_EDATA: return "data error";
	case DEFLATE_EMEM: return "insufficient memory";
	case DEFLATE_EBUF: return "buffer error";
	case DEFLATE_EUSAGE: return "libdeflate usage error";
	case DEFLATE_EINVAL: return "invalid argument";
	case DEFLATE_ECALLBACK: return "callback failure";
	}
}

static void realloc_shrink(void **ptr_ptr, size_t size)
{
	// Avoid realloc returning NULL on success for zero-size allocations by not
	// doing any such allocations.7c2a874e
	void *ptr;
	memcpy(&ptr, ptr_ptr, sizeof(void*)); // Strict aliasing.
	void *newptr = realloc(ptr, size ? size : 1);
	if ( newptr )
		memcpy(ptr_ptr, &newptr, sizeof(void*)); // Strict aliasing.
}

int deflate_easy_process(struct deflate_stream *stream,
                         const unsigned char *input,
                         size_t input_size,
                         unsigned char **output,
                         size_t *output_size)
{
	assert(stream);
	if ( !input || !output || !output_size )
		return DEFLATE_EINVAL;
	size_t result_used = 0;
	size_t result_size = 0;
	unsigned char *result = NULL;
	size_t input_used = 0;
	while ( true ) {
		if ( result_used == result_size ) {
			// TODO: Avoid multiplication overflow.
			size_t new_size = result_size ? 2 * result_size : 4096;
			unsigned char *new_result = realloc(result, new_size);
			if ( !new_result ) {
				free(result);
				return DEFLATE_EMEM;
			}
			result = new_result;
			result_size = new_size;
		}
		const unsigned char *in = input + input_used;
		size_t in_size = input_size - input_used;
		unsigned char *out = result + result_used;
		size_t out_size = result_size - result_used;
		size_t used;
		size_t made;
		int err = deflate_process(stream, in, in_size, &used, out, out_size,
		                          &made, DEFLATE_FLUSH_FINISH);
		if ( err != 0 && err != DEFLATE_STREAM_END ) {
			free(result);
			return err;
		}
		input_used += used;
		result_used += made;
		if ( err == DEFLATE_STREAM_END )
			break;
	}
	deflate_destroy(stream);
	realloc_shrink((void**) &result, result_used);
	*output = result;
	*output_size = result_used;
	return 0;
}

uint32_t deflate_adler32(uint32_t adler,
                         unsigned char *buffer,
                         size_t size)
{
	while ( size ) {
		size_t count = size;
		if ( UINT_MAX < count )
			count = UINT_MAX;
		adler = adler32(adler, buffer, count);
		size -= count;
	}
	return adler;
}

uint32_t deflate_adler32_combine(uint32_t adler1,
                                 uint32_t adler2,
                                 uintmax_t size2)
{
	return adler32_combine(adler1, adler2, size2 % 65521);
}

int deflate_easy_compress(const unsigned char *input,
                          size_t input_size,
                          unsigned char **output,
                          size_t *output_size,
                          int level)
{
	if ( !input || !output || !output_size )
		return DEFLATE_EINVAL;
	struct deflate_stream *stream = deflate_create_compress(level);
	if ( !stream )
		return DEFLATE_EMEM;
	int err = deflate_easy_process(stream, input, input_size, output, output_size);
	deflate_destroy(stream);
	return err;
}

int deflate_easy_decompress(const unsigned char *input,
                            size_t input_size,
                            unsigned char **output,
                            size_t *output_size)
{
	if ( !input || !output || !output_size )
		return DEFLATE_EINVAL;
	struct deflate_stream *stream = deflate_create_decompress();
	if ( !stream )
		return DEFLATE_EMEM;
	int err = deflate_easy_process(stream, input, input_size, output, output_size);
	deflate_destroy(stream);
	return err;
}
