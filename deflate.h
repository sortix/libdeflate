/*
 * Copyright (c) 2015 Jonas 'Sortie' Termansen
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef DEFLATE_H
#define DEFLATE_H

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

struct deflate_stream;

enum deflate_flush
{
	DEFLATE_FLUSH_NO = 0,
	DEFLATE_FLUSH_PARTIAL = 1,
	DEFLATE_FLUSH_SYNC = 2,
	DEFLATE_FLUSH_FULL = 3,
	DEFLATE_FLUSH_FINISH = 4,
	DEFLATE_FLUSH_BLOCK = 5,
	DEFLATE_FLUSH_TREES = 6,
};

#define DEFLATE_NEED_DICT 2
#define DEFLATE_STREAM_END 1
#define DEFLATE_EUNKNOWN (-1)
#define DEFLATE_ESTREAM (-2)
#define DEFLATE_EDATA (-3)
#define DEFLATE_EMEM (-4)
#define DEFLATE_EBUF (-5)
#define DEFLATE_EUSAGE (-6)
#define DEFLATE_EINVAL (-7)
#define DEFLATE_ECALLBACK (-8)

#define DEFLATE_LEVEL_DEFAULT (-1)
#define DEFLATE_LEVEL_NONE 0
#define DEFLATE_LEVEL_FAST 1
#define DEFLATE_LEVEL_BEST 9

struct deflate_stream *deflate_create(void);
struct deflate_stream *deflate_create_compress(int /*level*/);
struct deflate_stream *deflate_create_compress_raw(int /*level*/);
struct deflate_stream *deflate_create_decompress(void);
struct deflate_stream *deflate_create_decompress_raw(void);
void deflate_destroy(struct deflate_stream */*stream*/);
int deflate_begin_compress(struct deflate_stream */*stream*/);
int deflate_begin_compress_raw(struct deflate_stream */*stream*/);
int deflate_begin_decompress(struct deflate_stream */*stream*/);
int deflate_begin_decompress_raw(struct deflate_stream */*stream*/);
void deflate_set_level(struct deflate_stream */*stream*/,
                       int /*level*/);
int deflate_get_level(struct deflate_stream */*stream*/);
void deflate_set_strategy(struct deflate_stream */*stream*/,
                          int /*strategy*/);
int deflate_get_strategy(struct deflate_stream */*stream*/);
void deflate_set_windowbits(struct deflate_stream */*stream*/,
                            int /*windowbits*/);
int deflate_get_windowbits(struct deflate_stream */*stream*/);
void deflate_set_memlevel(struct deflate_stream */*stream*/,
                          int /*memlevel*/);
int deflate_get_memlevel(struct deflate_stream */*stream*/);
int deflate_set_dictionary(struct deflate_stream */*stream*/,
                           const unsigned char */*dictionary*/,
                           size_t dictionary_length);
int deflate_get_dictionary(struct deflate_stream */*stream*/,
                           unsigned char */*dictionary*/,
                           size_t */*dictionary_length*/);
uint32_t deflate_get_adler32(struct deflate_stream */*stream*/);
size_t deflate_compress_bound(struct deflate_stream */*stream*/,
                              size_t /*uncompressed_size*/);
int deflate_process(struct deflate_stream *stream,
                    const unsigned char */*input*/,
                    size_t /*input_size*/,
                    size_t */*input_used*/,
                    unsigned char */*output*/,
                    size_t /*output_size*/,
                    size_t */*output_used*/,
                    int /*flush_type*/);
int deflate_loop(struct deflate_stream */*stream*/,
                 void */*reader_context*/,
                 int (*/*reader*/)(void */*context*/,
                                   unsigned char */*buffer*/,
                                   size_t /*count*/,
                                   size_t */*transferred*/),
                 void */*writer_context*/,
                 int (*/*writer*/)(void */*context*/,
                                   const unsigned char */*buffer*/,
                                   size_t /*count*/,
                                   size_t */*transferred*/));
void deflate_reset(struct deflate_stream */*stream*/);
void deflate_end(struct deflate_stream */*stream*/);
const char *deflate_strerror(int /*errnum*/);
uint32_t deflate_adler32(uint32_t /*adler*/,
                         unsigned char */*buffer*/,
                         size_t /*size*/);
uint32_t deflate_adler32_combine(uint32_t /*adler1*/,
                                 uint32_t /*adler2*/,
                                 uintmax_t /*size2*/);
int deflate_easy_process(struct deflate_stream */*stream*/,
                         const unsigned char */*input*/,
                         size_t /*input_size*/,
                         unsigned char **/*output*/,
                         size_t */*output_size*/);
int deflate_easy_compress(const unsigned char */*input*/,
                          size_t /*input_size*/,
                          unsigned char **/*output*/,
                          size_t */*output_size*/,
                          int level);
int deflate_easy_decompress(const unsigned char */*input*/,
                            size_t /*input_size*/,
                            unsigned char **/*output*/,
                            size_t */*output_size*/);

#ifdef __cplusplus
}
#endif

#endif
