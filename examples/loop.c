#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <deflate.h>

int reader(void *context,
           unsigned char *buffer,
           size_t count,
           size_t *transferred) {
	FILE *fp = (FILE *)context;
	*transferred = fread(buffer, 1, count, fp);
	if ( ferror(fp) )
		return -1;
	if ( feof(fp) )
		return DEFLATE_STREAM_END;
	return 0;
}

int writer(void *context,
           const unsigned char *buffer,
           size_t count,
           size_t *transferred) {
	FILE *fp = (FILE *)context;
	*transferred = fwrite(buffer, 1, count, fp);
	if ( ferror(fp) )
		return -1;
	return 0;
}

int main(int argc, char *argv[])
{
	int level = DEFLATE_LEVEL_DEFAULT;
	bool compress = true;
	for ( int i = 0; i < argc; i++ ) {
		const char *arg = argv[i];
		if ( arg[0] != '-' || !arg[1] )
			continue;
		argv[i] = NULL;
		if ( !strcmp(arg, "--") )
			break;
		for ( size_t i = 1; arg[i]; i++ ) {
			switch ( arg[i] ) {
			case '0': level = 0; break;
			case '1': level = 1; break;
			case '2': level = 2; break;
			case '3': level = 3; break;
			case '4': level = 4; break;
			case '5': level = 5; break;
			case '6': level = 6; break;
			case '7': level = 7; break;
			case '8': level = 8; break;
			case '9': level = 9; break;
			case 'c': compress = true; break;
			case 'd': compress = false; break;
			default:
				fprintf(stderr, "%s: unknown option -- '%c'\n", argv[0], arg[i]);
				exit(1);
			}
		}
	}
	int ret;
	struct deflate_stream *stream = deflate_create();
	if ( !stream ) {
		fprintf(stderr, "%s: malloc failure\n", argv[0]);
		exit(1);
	}
	if ( compress ) {
		deflate_set_level(stream, level);
		ret = deflate_begin_compress(stream);
	} else
		ret = deflate_begin_decompress(stream);
	if ( ret < 0 ) {
		fprintf(stderr, "%s: begin: %s\n", argv[0], deflate_strerror(ret));
		exit(1);
	}
	if ( (ret = deflate_loop(stream, stdin, reader, stdout, writer)) < 0 ) {
		fprintf(stderr, "%s: loop: %s\n", argv[0], deflate_strerror(ret));
		exit(1);
	}
	deflate_destroy(stream);
	if ( ferror(stdout) || fflush(stdout) == EOF ) {
		fprintf(stderr, "%s: stdout write failure\n", argv[0]);
		exit(1);
	}
	return 0;
}
